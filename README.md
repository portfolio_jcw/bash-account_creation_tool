# Bash-Account_Creation_Tool

Created a lightweight proof-of-concept, modular Bash-scripted tool for automating the creation of user accounts and group membership in a Linux environment.