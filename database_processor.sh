#!/bin/bash
#											#
# This script uses the ubuild.sh script to build a bunch of new users from a file	#
#											#

for LINE in $(seq 1 25)					# limited to 25 users
do
	USER_LINE=$(awk "NR==$LINE" userblock.txt)

	if [ ${#USER_LINE} -eq 0 ]			# break if no more user lines
	then
		break
	fi

	eval "./ubuild.sh $USER_LINE"			# execute the ubuild.sh script on each user line
done
