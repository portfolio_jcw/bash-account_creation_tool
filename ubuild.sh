#!/bin/bash
#										#
# This script will automate creation of user accounts and assignment to groups	#
#										#

# Create user account from 1st arg with password from 2nd arg

useradd $1 -p $2

# Add user to groups from 3rd to 10th args, break loop if no more args

for COUNT in $(seq 3 10)
do
	eval GROUP="$"$COUNT""
	if [ -z $GROUP ]
	then
		break
	fi
	usermod -aG $GROUP $1
done

# confirm user and groups

id $1
