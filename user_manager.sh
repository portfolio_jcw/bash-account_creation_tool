#!/bin/bash

# 								#
# Script to build database and then turn it into user accounts	#
#								#

rm userblock.txt && touch userblock.txt				# clear database
MORE="y"
echo "\nBuilding user database..."

while [ $MORE = "y" ]						# user input loop
do
	echo -n "\nNew user: " ; read USERNAME
	echo -n "Password: " ; read PASSW0RD
	echo -n "Groups, separated by spaces: " ; read GROUPZ

	USER_LINE="$USERNAME $PASSW0RD $GROUPZ"
	echo $USER_LINE >> userblock.txt			# append user lines

	echo -n "\nAnother user (y/n)? " ; read MORE
done

echo "\n\nCreating user accounts..."
./database_processor.sh							# run script to convert database
echo "\n\nComplete"


